def possible_count(array)
  options = 0

  (1..array.size - 1).to_a.each do |i|
    if (array[i - 1] > array[i])
      return 0 if (options != 0)
      options += 1 if ((i == 1) || (array[i - 2] <= array[i]))
      options += 1 if ((i == array.size - 1) || (array[i - 1] <= array[i + 1]))
      return 0 if (options == 0)
    end
  end

  return (options == 0) ? array.size : options
end

a = [1, 4, 5, 6, 9]
puts possible_count(a)
a = [7, 8, 9, 8]
puts possible_count(a)
a = [10, 5, 4, 1]
puts possible_count(a)
